Please remember to add all infrastructures to the lock file when creating it or after upgrading:
```bash
terraform providers lock -platform=windows_amd64 -platform=darwin_amd64 -platform=linux_amd64
```

GCP

Log in a way that terraform can use it to access systems:

https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started

create first tfstate resources process:

https://cloud.google.com/docs/terraform/resource-management/store-state

AZURE

https://learn.microsoft.com/en-us/azure/developer/terraform/store-state-in-azure-storage?tabs=azure-cli

AWS account: 051743034439

TODO: import project `coe-devops-cloud-sandbox` to `gcp/mgmt/global/projects`
TODO: domain was created by accident in the `coe-devops-cloud-sandbox` but needs to be stored in the `gcp/mgmt/global/domain`
TODO: for AWS account we have We need policy: https://repost.aws/knowledge-center/mfa-iam-user-aws-cli and we will need to create users in form name.lastname.mobica with this policy added. We need also to find documentation for using Yubikey as and MFA device - I done this once but I need to refresh my memory