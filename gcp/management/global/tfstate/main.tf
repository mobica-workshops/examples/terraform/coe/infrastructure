module "tfstate" {
  source   = "../../../../../modules/gcp/management/global/tfstate"

  location = "EUROPE-CENTRAL2"
}

output "tfstate" {
  value = module.tfstate.tfstate
}