output "cloud_name_servers" {
  value = google_dns_managed_zone.cloud_mobica_com.name_servers
}

output "gcp_cloud_name_servers" {
  value = google_dns_managed_zone.gcp_cloud_mobica_com.name_servers
}

output "gcp_cloud_name" {
  value = google_dns_managed_zone.gcp_cloud_mobica_com.name
}

output "gcp_cloud_dns_name" {
  value = google_dns_managed_zone.gcp_cloud_mobica_com.dns_name
}