terraform {
  required_version = "~> 1"
  backend "gcs" {
    bucket  = "coe-devops-cloud-admin-ca18429efb894d39-tfstate"
    prefix  = "gcp/management/global/dns"
  }
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "~> 4"
    }
  }
}
