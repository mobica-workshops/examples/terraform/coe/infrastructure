data "google_project" "project" {
}

resource "google_dns_managed_zone" "cloud_mobica_com" {
  name        = "cloud-mobica-com"
  dns_name    = "cloud.mobica.com."
  description = "CoE DevOps/Cloud main zone"
  project     = data.google_project.project.project_id
}

resource "google_dns_managed_zone" "gcp_cloud_mobica_com" {
  name        = "gcp-cloud-mobica-com"
  dns_name    = "gcp.cloud.mobica.com."
  description = "CoE DevOps/Cloud GCP zone"
  project     = data.google_project.project.project_id
}

resource "google_dns_record_set" "cloud_mobica_com_gcp_NS" {
  name         = google_dns_managed_zone.gcp_cloud_mobica_com.dns_name
  managed_zone = google_dns_managed_zone.cloud_mobica_com.name
  type         = "NS"
  ttl          = 3600

  rrdatas = google_dns_managed_zone.gcp_cloud_mobica_com.name_servers
  project = data.google_project.project.project_id
}
