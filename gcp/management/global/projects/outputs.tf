output "management_project_id" {
  value = data.google_project.project.project_id
}

output "coe_devops_cloud_sandbox_project_id" {
  value = google_project.coe_devops_cloud_sandbox.project_id
}

output "coe_devops_cloud_sandbox_project_number" {
  value = google_project.coe_devops_cloud_sandbox.number
}

# This is something more like a variable for the other projects what needs to be unique only internally
output "coe_devops_cloud_sandbox_project_short_name" {
  value = "sandbox"
}