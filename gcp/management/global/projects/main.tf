data "google_project" "project" {}

resource "google_project" "coe_devops_cloud_sandbox" {
  name            = "coe-devops-cloud-sandbox"
  project_id      = "coe-devops-cloud-sandbox"
  billing_account = var.billing_account
  folder_id       = var.folder_id
}
