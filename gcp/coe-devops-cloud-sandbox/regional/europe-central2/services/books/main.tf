resource "kubernetes_secret_v1" "secret-staging" {
  metadata {
    name      = "books-frontend-staging-ssl"
    namespace = "workshops-staging"
  }

  data = {
    "tls.key" = ""
    "tls.crt" = ""
  }

  type = "kubernetes.io/tls"

  lifecycle {
    ignore_changes = [
      metadata,
      timeouts
    ]
  }
}

resource "kubernetes_secret_v1" "secret-production" {
  metadata {
    name      = "books-frontend-production-ssl"
    namespace = "workshops-production"
  }

  data = {
    "tls.key" = ""
    "tls.crt" = ""
  }

  type = "kubernetes.io/tls"

  lifecycle {
    ignore_changes = [
      metadata,
      timeouts
    ]
  }
}
