output "region" {
  value       = module.core.region
  description = "GCloud Region"
}

output "project_id" {
  value       = module.core.project_id
  description = "GCloud Project ID"
}

output "kubernetes_cluster_name" {
  value       = module.core.kubernetes_cluster_name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = module.core.kubernetes_cluster_host
  description = "GKE Cluster Host"
}

output "kubernetes_cluster_client_certificate" {
  value = module.core.kubernetes_cluster_client_certificate
  sensitive = true
}

output "kubernetes_cluster_client_key" {
  value = module.core.kubernetes_cluster_client_key
  sensitive = true
}

output "kubernetes_cluster_ca_certificate" {
  value = module.core.kubernetes_cluster_ca_certificate
  sensitive = true
}