# Encrypt
# sops --encrypt --gcp-kms projects/coe-devops-cloud-sandbox/locations/europe/keyRings/sops/cryptoKeys/sops-key my-secrets.yml > my-secrets.enc.yml

data "sops_file" "demo-secret" {
  source_file = "my-secrets.enc.yml"
  input_type  = "yaml"
}

module "workshops-staging" {
  source           = "../../../../../../../modules/gcp/environments/regional/core/postgresql"
  namespace        = "workshops-staging"
  rootPassword     = data.sops_file.demo-secret.data.mobica-workshops-staging-postgresql-rootPassword
  bookListPassword = data.sops_file.demo-secret.data.mobica-workshops-staging-postgresql-bookListPassword
}

module "workshops-production" {
  source           = "../../../../../../../modules/gcp/environments/regional/core/postgresql"
  namespace        = "workshops-production"
  rootPassword     = data.sops_file.demo-secret.data.mobica-workshops-production-postgresql-rootPassword
  bookListPassword = data.sops_file.demo-secret.data.mobica-workshops-production-postgresql-bookListPassword
}
