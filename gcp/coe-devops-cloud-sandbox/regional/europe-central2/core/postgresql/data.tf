data "terraform_remote_state" "gke" {
  backend = "gcs"

  config = {
    bucket = "coe-devops-cloud-admin-ca18429efb894d39-tfstate"
    prefix = "gcp/coe-devops-cloud-sandbox/regional/europe-central2/core"
  }
}