# Encrypt
# sops --encrypt --gcp-kms projects/coe-devops-cloud-sandbox/locations/europe/keyRings/sops/cryptoKeys/sops-key my-secrets.yml > my-secrets.enc.yml

data "sops_file" "demo-secret" {
  source_file = "my-secrets.enc.yml"
  input_type  = "yaml"
}

module "workshops-staging" {
  source            = "../../../../../../../modules/gcp/environments/regional/core/mongodb"
  namespace         = "workshops-staging"
  rootPassword      = data.sops_file.demo-secret.data.mobica-workshops-staging-mongodb-rootPassword
  bookAdminPassword = data.sops_file.demo-secret.data.mobica-workshops-staging-mongodb-bookAdminPassword
}

module "workshops-production" {
  source            = "../../../../../../../modules/gcp/environments/regional/core/mongodb"
  namespace         = "workshops-production"
  rootPassword      = data.sops_file.demo-secret.data.mobica-workshops-production-mongodb-rootPassword
  bookAdminPassword = data.sops_file.demo-secret.data.mobica-workshops-production-mongodb-bookAdminPassword
}
