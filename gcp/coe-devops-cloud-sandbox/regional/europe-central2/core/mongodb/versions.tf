terraform {
  required_version = "~> 1"
  backend "gcs" {
    bucket  = "coe-devops-cloud-admin-ca18429efb894d39-tfstate"
    prefix  = "gcp/coe-devops-cloud-sandbox/regional/europe-central2/core/mongodb"
  }
  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2"
    }
    sops = {
      source = "carlpett/sops"
    }
  }
}