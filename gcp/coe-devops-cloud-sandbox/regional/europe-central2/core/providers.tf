provider "google" {
  project = "coe-devops-cloud-sandbox"
  region  = "europe-central2"
  zone    = "europe-central2-b"
}
