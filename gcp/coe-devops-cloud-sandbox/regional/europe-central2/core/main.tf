// gcloud container clusters get-credentials $(terraform output -raw kubernetes_cluster_name) --region $(terraform output -raw region) --project $(terraform output -raw project_id)
// gcloud container clusters update --disable-managed-prometheus $(terraform output -raw kubernetes_cluster_name) --region $(terraform output -raw region) --project $(terraform output -raw project_id)
module "core" {
  source   = "../../../../../../modules/gcp/environments/regional/core"
}
