data "terraform_remote_state" "management_global_dns" {
  backend = "gcs"

  config = {
    bucket = "coe-devops-cloud-admin-ca18429efb894d39-tfstate"
    prefix = "gcp/management/global/dns"
  }
}

data "terraform_remote_state" "management_global_projects" {
  backend = "gcs"

  config = {
    bucket = "coe-devops-cloud-admin-ca18429efb894d39-tfstate"
    prefix = "gcp/management/global/projects"
  }
}
