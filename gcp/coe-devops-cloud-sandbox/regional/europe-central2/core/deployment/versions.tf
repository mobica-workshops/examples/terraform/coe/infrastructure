terraform {
  required_version = "~> 1"
  backend "gcs" {
    bucket  = "coe-devops-cloud-admin-ca18429efb894d39-tfstate"
    prefix  = "gcp/coe-devops-cloud-sandbox/regional/europe-central2/core/deployment"
  }
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~> 2"
    }
    local = {
      source = "hashicorp/local"
      version = "~> 2"
    }
  }
}
