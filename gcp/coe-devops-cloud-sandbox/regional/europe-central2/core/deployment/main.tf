module "staging_deployment" {
  source                            = "../../../../../../../modules/gcp/environments/regional/core/deployment"
  namespace                         = "workshops-staging"
  service_account_name              = "gitlab-ci"
  kubernetes_cluster_name           = data.terraform_remote_state.gke.outputs.kubernetes_cluster_name
  kubernetes_cluster_host           = data.terraform_remote_state.gke.outputs.kubernetes_cluster_host
  kubernetes_cluster_ca_certificate = data.terraform_remote_state.gke.outputs.kubernetes_cluster_ca_certificate
}

module "production_deployment" {
  source                            = "../../../../../../../modules/gcp/environments/regional/core/deployment"
  namespace                         = "workshops-production"
  service_account_name              = "gitlab-ci"
  kubernetes_cluster_name           = data.terraform_remote_state.gke.outputs.kubernetes_cluster_name
  kubernetes_cluster_host           = data.terraform_remote_state.gke.outputs.kubernetes_cluster_host
  kubernetes_cluster_ca_certificate = data.terraform_remote_state.gke.outputs.kubernetes_cluster_ca_certificate
}
