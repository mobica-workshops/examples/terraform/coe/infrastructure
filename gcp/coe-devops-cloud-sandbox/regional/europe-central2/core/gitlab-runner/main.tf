# Encrypt
# sops --encrypt --gcp-kms projects/coe-devops-cloud-sandbox/locations/europe/keyRings/sops/cryptoKeys/sops-key my-secrets.yml > my-secrets.enc.yml

data "sops_file" "demo-secret" {
  source_file = "my-secrets.enc.yml"
  input_type  = "yaml"
}

output "root-value-password" {
  # Access the password variable from the map
  sensitive = true
  value     = data.sops_file.demo-secret.data.mobica-workshops-gitlab-runner-token
}

module "workshops" {
  source     = "../../../../../../../modules/gcp/environments/regional/core/gitlab-runner"
  namespace  = "workshops-gitlab-ci"
  token      = data.sops_file.demo-secret.data.mobica-workshops-gitlab-runner-token
  replicas   = 2
  concurrent = 4
}
