provider "kubernetes" {
  host = "https://${data.terraform_remote_state.gke.outputs.kubernetes_cluster_host}"

  client_certificate     = base64decode(data.terraform_remote_state.gke.outputs.kubernetes_cluster_client_certificate)
  client_key             = base64decode(data.terraform_remote_state.gke.outputs.kubernetes_cluster_client_key)
  cluster_ca_certificate = base64decode(data.terraform_remote_state.gke.outputs.kubernetes_cluster_ca_certificate)

  exec {
    api_version = "client.authentication.k8s.io/v1beta1"
    command     = "gke-gcloud-auth-plugin"
  }
}

provider "helm" {
  kubernetes {
    host = "https://${data.terraform_remote_state.gke.outputs.kubernetes_cluster_host}"

    client_certificate     = base64decode(data.terraform_remote_state.gke.outputs.kubernetes_cluster_client_certificate)
    client_key             = base64decode(data.terraform_remote_state.gke.outputs.kubernetes_cluster_client_key)
    cluster_ca_certificate = base64decode(data.terraform_remote_state.gke.outputs.kubernetes_cluster_ca_certificate)

    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command = "gke-gcloud-auth-plugin"
    }
  }
}
