module "issuer-workshops-staging-staging" {
  source       = "../../../../../../../../modules/gcp/environments/regional/core/cert-manager/issuer"
  namespace    = "workshops-staging"
  issuer_name  = "letsencrypt-staging"
  ingress_name = "book-frontend"
  server       = "https://acme-staging-v02.api.letsencrypt.org/directory"
}

module "issuer-workshops-staging-production" {
  source       = "../../../../../../../../modules/gcp/environments/regional/core/cert-manager/issuer"
  namespace    = "workshops-staging"
  ingress_name = "book-frontend"
}

module "issuer-workshops-production-staging" {
  source       = "../../../../../../../../modules/gcp/environments/regional/core/cert-manager/issuer"
  namespace    = "workshops-production"
  issuer_name  = "letsencrypt-staging"
  ingress_name = "book-frontend"
  server       = "https://acme-staging-v02.api.letsencrypt.org/directory"
}

module "issuer-workshops-production-production" {
  source       = "../../../../../../../../modules/gcp/environments/regional/core/cert-manager/issuer"
  namespace    = "workshops-production"
  ingress_name = "book-frontend"
}
