# Account used in the GitlabCI to send images to the Artifact Registry
resource "google_service_account" "artifact_registry_service" {
  account_id   = "artifact-registry-service"
  display_name = "Artifact Registry Service"
}

output "artifact_registry_service_account_email" {
  value = google_service_account.artifact_registry_service.email
}

resource "google_artifact_registry_repository" "docker" {
  repository_id = "docker"
  description   = "our docker repository"
  format        = "DOCKER"
}

resource "google_artifact_registry_repository_iam_binding" "docker" {
  project    = google_artifact_registry_repository.docker.project
  location   = google_artifact_registry_repository.docker.location
  repository = google_artifact_registry_repository.docker.name
  role       = "roles/artifactregistry.writer"
  members    = [
    "serviceAccount:${google_service_account.artifact_registry_service.email}",
  ]
}

resource "google_artifact_registry_repository" "charts" {
  repository_id = "charts"
  description   = "Helm charts repository"
  format        = "DOCKER"
}

resource "google_artifact_registry_repository_iam_binding" "charts" {
  project    = google_artifact_registry_repository.charts.project
  location   = google_artifact_registry_repository.charts.location
  repository = google_artifact_registry_repository.charts.name
  role       = "roles/artifactregistry.writer"
  members    = [
    "serviceAccount:${google_service_account.artifact_registry_service.email}",
  ]
}
