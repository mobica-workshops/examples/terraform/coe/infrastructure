resource "google_dns_record_set" "staging_api" {
  name         = "staging-books-api.${data.terraform_remote_state.sandbox_global_dns.outputs.project_cloud_dns_name}"
  managed_zone = data.terraform_remote_state.sandbox_global_dns.outputs.project_cloud_name
  type         = "A"
  ttl          = 300

  rrdatas = [data.terraform_remote_state.sandbox_global_services.outputs.mobica_workshops_staging_address]
}

resource "google_dns_record_set" "staging_frontend" {
  name         = "staging-books.${data.terraform_remote_state.sandbox_global_dns.outputs.project_cloud_dns_name}"
  managed_zone = data.terraform_remote_state.sandbox_global_dns.outputs.project_cloud_name
  type         = "A"
  ttl          = 300

  rrdatas = [data.terraform_remote_state.sandbox_global_services.outputs.mobica_workshops_staging_address]
}

resource "google_dns_record_set" "api" {
  name         = "books-api.${data.terraform_remote_state.sandbox_global_dns.outputs.project_cloud_dns_name}"
  managed_zone = data.terraform_remote_state.sandbox_global_dns.outputs.project_cloud_name
  type         = "A"
  ttl          = 300

  rrdatas = [data.terraform_remote_state.sandbox_global_services.outputs.mobica_workshops_production_address]
}

resource "google_dns_record_set" "frontend" {
  name         = "books.${data.terraform_remote_state.sandbox_global_dns.outputs.project_cloud_dns_name}"
  managed_zone = data.terraform_remote_state.sandbox_global_dns.outputs.project_cloud_name
  type         = "A"
  ttl          = 300

  rrdatas = [data.terraform_remote_state.sandbox_global_services.outputs.mobica_workshops_production_address]
}