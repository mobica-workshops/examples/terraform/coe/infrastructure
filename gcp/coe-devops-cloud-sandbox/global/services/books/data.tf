data "terraform_remote_state" "sandbox_global_dns" {
  backend = "gcs"

  config = {
    bucket = "coe-devops-cloud-admin-ca18429efb894d39-tfstate"
    prefix = "gcp/coe-devops-cloud-sandbox/global/dns"
  }
}

data "terraform_remote_state" "sandbox_global_services" {
  backend = "gcs"

  config = {
    bucket = "coe-devops-cloud-admin-ca18429efb894d39-tfstate"
    prefix = "gcp/coe-devops-cloud-sandbox/global/services"
  }
}
