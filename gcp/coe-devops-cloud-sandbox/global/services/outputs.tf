output "mobica_workshops_staging_address" {
  value = google_compute_global_address.mobica_workshops_staging.address
}

output "mobica_workshops_production_address" {
  value = google_compute_global_address.mobica_workshops_production.address
}