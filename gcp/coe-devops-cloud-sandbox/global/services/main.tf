resource "google_compute_global_address" "mobica_workshops_staging" {
  name = "mobica-workshops-staging"
}

resource "google_compute_global_address" "mobica_workshops_production" {
  name = "mobica-workshops-production"
}
