output "project_cloud_name" {
  value = module.dns.project_cloud_name
}

output "project_cloud_dns_name" {
  value = module.dns.project_cloud_dns_name
}