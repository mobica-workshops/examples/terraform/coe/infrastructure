module "dns" {
  source   = "../../../../../modules/gcp/environments/global/dns"

  management_project_id = data.terraform_remote_state.management_global_projects.outputs.management_project_id
  project_short_name    = data.terraform_remote_state.management_global_projects.outputs.coe_devops_cloud_sandbox_project_short_name
  gcp_cloud_dns_name    = data.terraform_remote_state.management_global_dns.outputs.gcp_cloud_dns_name
  gcp_cloud_name        = data.terraform_remote_state.management_global_dns.outputs.gcp_cloud_name
}
