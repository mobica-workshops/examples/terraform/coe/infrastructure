terraform {
  required_version = "~> 1"
  backend "azurerm" {
    resource_group_name  = "coe-devops-cloud"
    storage_account_name = "coedevopscloxxllftfstate"
    container_name       = "coe-devops-cloud-tfstate"
    key                  = "azure/management/global/tfstate/terraform.tfstate"
  }
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3"
    }
  }
}

provider "azurerm" {
  features {}
}