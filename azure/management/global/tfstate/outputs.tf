output "storage_account_name" {
  value = module.tfstate.storage_account_name
}

output "storage_container_name" {
  value = module.tfstate.storage_container_name
}