module "tfstate" {
  source   = "../../../../../modules/azure/management/global/tfstate"
  location = "westeurope"
  rg_name  = "coe-devops-cloud"
}
